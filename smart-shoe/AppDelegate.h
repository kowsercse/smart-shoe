//
//  AppDelegate.h
//  smart-shoe
//
//  Created by Kowser on 11/26/14.
//  Copyright (c) 2014 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WifiStreamReader.h"
#import "GraphDataSource.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
  CPTGraph *graph;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) WifiStreamReader *streamReader;
@property (strong, nonatomic) GraphDataSource *graphDataSource;
@property (strong, nonatomic) CPTGraphHostingView *hostView;

@property(nonatomic, strong) NSMutableDictionary *plotColors;
@end

