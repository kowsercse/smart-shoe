//
//  AppDelegate.m
//  smart-shoe
//
//  Created by Kowser on 11/26/14.
//  Copyright (c) 2014 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [self createColors];
  self.graphDataSource = [[GraphDataSource alloc] initWithColors:self.plotColors];
  self.hostView = [[CPTGraphHostingView alloc] initWithFrame:self.window.bounds];
  [self initPlot];
  self.hostView.hostedGraph = graph;
  self.streamReader = [[WifiStreamReader alloc] initWithGraph:graph graphDataSource:self.graphDataSource];

  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
  // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
  // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)initPlot
{
  graph = [[CPTXYGraph alloc] initWithFrame:self.window.bounds];
  [graph applyTheme:[CPTTheme themeNamed:kCPTSlateTheme]];
  [self prepareTitle];
  [self setPaddingFor];

  [self configurePlots];
  [self configureAxes];
}

- (void)configurePlots
{
  CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
  NSArray *plots = [self createPlots:plotSpace];
  [plotSpace scaleToFitPlots:plots];
  CPTMutablePlotRange *xRange = (CPTMutablePlotRange *) [plotSpace.xRange mutableCopy];
  [xRange expandRangeByFactor:CPTDecimalFromCGFloat(1.1f)];
  plotSpace.xRange = xRange;
  CPTMutablePlotRange *yRange = (CPTMutablePlotRange *) [plotSpace.yRange mutableCopy];
  [yRange expandRangeByFactor:CPTDecimalFromCGFloat(1.2f)];
  plotSpace.yRange = yRange;
}

- (NSArray *)createPlots:(CPTXYPlotSpace *)plotSpace
{
  NSMutableArray *plots = [NSMutableArray array];
  for (id key in self.plotColors) {
    CPTColor *color = self.plotColors[key];
    CPTScatterPlot *plot = [self createPlot:color identifier:key];
    [graph addPlot:plot toPlotSpace:plotSpace];
    [plots addObject:plot];
  }

  return plots;
}

- (CPTScatterPlot *)createPlot:(CPTColor *)lineColor identifier:(NSString *const)identifier
{
  CPTScatterPlot *plot = [[CPTScatterPlot alloc] init];

  CPTMutableLineStyle *lineColorStyle = [CPTMutableLineStyle lineStyle];
  lineColorStyle.lineColor = lineColor;

  CPTPlotSymbol *plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
  plotSymbol.fill = [CPTFill fillWithColor:lineColor];
  plotSymbol.lineStyle = lineColorStyle;
  plotSymbol.size = CGSizeMake(6.0f, 6.0f);

  CPTMutableLineStyle *lineDataStyle = (CPTMutableLineStyle *) [plot.dataLineStyle mutableCopy];
  lineDataStyle.lineWidth = 2.5;
  lineDataStyle.lineColor = lineColor;

  plot.dataSource = self.graphDataSource;
  plot.identifier = identifier;
  plot.dataLineStyle = lineDataStyle;
  plot.plotSymbol = plotSymbol;

  return plot;
}

- (void)configureAxes
{
  // 1 - Create styles
  CPTMutableTextStyle *axisTitleStyle = [CPTMutableTextStyle textStyle];
  axisTitleStyle.color = [CPTColor whiteColor];
  axisTitleStyle.fontName = @"Helvetica-Bold";
  axisTitleStyle.fontSize = 12.0f;

  CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
  axisLineStyle.lineWidth = 2.0f;
  axisLineStyle.lineColor = [CPTColor whiteColor];

  CPTMutableTextStyle *axisTextStyle = [[CPTMutableTextStyle alloc] init];
  axisTextStyle.color = [CPTColor whiteColor];
  axisTextStyle.fontName = @"Helvetica-Bold";
  axisTextStyle.fontSize = 11.0f;

  CPTMutableLineStyle *tickLineStyle = [CPTMutableLineStyle lineStyle];
  tickLineStyle.lineColor = [CPTColor whiteColor];
  tickLineStyle.lineWidth = 2.0f;

  CPTMutableLineStyle *gridLineStyle = [CPTMutableLineStyle lineStyle];
  tickLineStyle.lineColor = [CPTColor blackColor];
  tickLineStyle.lineWidth = 1.0f;

  // 2 - Get axis set
  CPTXYAxisSet *axisSet = (CPTXYAxisSet *) graph.axisSet;

  // 3 - Configure x-axis
  CPTAxis *x = axisSet.xAxis;
  x.title = @"Day of Month";
  x.titleTextStyle = axisTitleStyle;
  x.titleOffset = 15.0f;
  x.axisLineStyle = axisLineStyle;
  x.labelingPolicy = CPTAxisLabelingPolicyNone;
  x.labelTextStyle = axisTextStyle;
  x.majorTickLineStyle = axisLineStyle;
  x.majorTickLength = 1.0f;
  x.tickDirection = CPTSignNegative;

  NSMutableSet *xLabels = [NSMutableSet setWithCapacity:MAX_DATA];
  NSMutableSet *xLocations = [NSMutableSet setWithCapacity:MAX_DATA];
  for (int i=1; i<31; i++) {
    CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:[@(i) stringValue] textStyle:x.labelTextStyle];
    CGFloat location = i;
    label.tickLocation = CPTDecimalFromCGFloat(location);
    label.offset = x.majorTickLength;
    if (label) {
      [xLabels addObject:label];
      [xLocations addObject:@(location)];
    }
  }
  x.axisLabels = xLabels;
  x.majorTickLocations = xLocations;


  // 4 - Configure y-axis
  CPTAxis *y = axisSet.yAxis;
  y.title = @"Price";
  y.titleTextStyle = axisTitleStyle;
  y.titleOffset = -40.0f;
  y.axisLineStyle = axisLineStyle;
  y.majorGridLineStyle = gridLineStyle;
  y.labelingPolicy = CPTAxisLabelingPolicyNone;
  y.labelTextStyle = axisTextStyle;
  y.labelOffset = 16.0f;
  y.majorTickLineStyle = axisLineStyle;
  y.majorTickLength = 4.0f;
  y.minorTickLength = 2.0f;
  y.tickDirection = CPTSignPositive;

  NSInteger majorIncrement = 100;
  NSInteger minorIncrement = 50;
  CGFloat yMax = 700.0f;  // should determine dynamically based on max price
  NSMutableSet *yLabels = [NSMutableSet set];
  NSMutableSet *yMajorLocations = [NSMutableSet set];
  NSMutableSet *yMinorLocations = [NSMutableSet set];
  for (NSInteger j = minorIncrement; j <= yMax; j += minorIncrement) {
    NSUInteger mod = j % majorIncrement;
    if (mod == 0) {
      CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:[NSString stringWithFormat:@"%i", j] textStyle:y.labelTextStyle];
      NSDecimal location = CPTDecimalFromInteger(j);
      label.tickLocation = location;
      label.offset = -y.majorTickLength - y.labelOffset;
      if (label) {
        [yLabels addObject:label];
      }
      [yMajorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:location]];
    } else {
      [yMinorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:CPTDecimalFromInteger(j)]];
    }
  }
  y.axisLabels = yLabels;
  y.majorTickLocations = yMajorLocations;
  y.minorTickLocations = yMinorLocations;
}

// 4 - Set padding for plot area
- (void)setPaddingFor
{
  [graph.plotAreaFrame setPaddingLeft:30.0f];
  [graph.plotAreaFrame setPaddingBottom:30.0f];
}

- (void)prepareTitle
{
  graph.title = @"Portfolio Prices: April 2012";
  graph.titleTextStyle = [self getStyle];
  graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
  graph.titleDisplacement = CGPointMake(0.0f, 10.0f);
}

- (CPTMutableTextStyle *)getStyle
{
  CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
  titleStyle.color = [CPTColor whiteColor];
  titleStyle.fontName = @"Helvetica-Bold";
  titleStyle.fontSize = 16.0f;
  return titleStyle;
}

- (NSMutableDictionary *)createColors
{
  self.plotColors = [NSMutableDictionary dictionary];
  self.plotColors[@"rotX"] = [CPTColor redColor];
  self.plotColors[@"rotY"] = [CPTColor yellowColor];
  self.plotColors[@"rotZ"] = [CPTColor orangeColor];
    
  self.plotColors[@"accX"] = [CPTColor greenColor];

  self.plotColors[@"pressure1"] = [CPTColor blueColor];
  return self.plotColors;
}

@end
