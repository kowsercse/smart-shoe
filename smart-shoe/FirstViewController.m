//
//  FirstViewController.m
//  smart-shoe
//
//  Created by Kowser on 11/26/14.
//  Copyright (c) 2014 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
