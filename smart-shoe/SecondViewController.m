//
//  SecondViewController.m
//  smart-shoe
//
//  Created by Kowser on 11/26/14.
//  Copyright (c) 2014 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "SecondViewController.h"
#import "AppDelegate.h"

@implementation SecondViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  [self.view addSubview:appDelegate.hostView];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
