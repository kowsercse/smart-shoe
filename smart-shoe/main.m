//
//  main.m
//  smart-shoe
//
//  Created by Kowser on 11/26/14.
//  Copyright (c) 2014 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
