//
// Created by Kowser on 11/23/14.
// Copyright (c) 2014 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CorePlot-CocoaTouch.h"

static int const MAX_DATA = 30;

@interface GraphDataSource : NSObject<CPTPlotDataSource> {
  NSMutableDictionary *data;
}

-(void)addDataForPlot:(id)identifier data:(double)value;

- (id)initWithColors:(NSMutableDictionary *)dictionary;

@end