//
// Created by Kowser on 11/23/14.
// Copyright (c) 2014 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "GraphDataSource.h"


@implementation GraphDataSource

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
  return 30;
}

- (id)initWithColors:(NSMutableDictionary *)plotColors
{
  self = [super init];
  if (self) {
    data = [NSMutableDictionary dictionary];
    for(id key in plotColors) {
      NSMutableArray *array = [NSMutableArray array];
      data[key] = array;

      for(NSUInteger i=0; i<30; i++) {
        array[i] = @0.0f;
      }
    }
  }
  return self;
}

-(double)doubleForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
  if(fieldEnum == 0) {
    return index;
  }
  else {
    NSArray *array = data[plot.identifier];
    return [array[index] doubleValue];
  }
}

-(void)addDataForPlot:(id)identifier data:(double)value {
  NSMutableArray *array = data[identifier];
  [array removeObjectAtIndex:0];
  [array addObject:@(value)];
}

@end