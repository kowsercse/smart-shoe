//
// Created by Kowser on 11/26/14.
// Copyright (c) 2014 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import "SimplePing.h"
#import "CorePlot-CocoaTouch.h"
#import "GraphDataSource.h"


@interface WifiStreamReader : NSObject <SimplePingDelegate, NSStreamDelegate> {
  NSString *csvFilePath;
  NSInputStream *inpStream;
  NSOutputStream *outputStream;
  NSMutableString *stringBuffer;

  SimplePing *simplePing;
  CMMotionManager *motionManager;
  GraphDataSource *graphDataSource;
  CPTGraph *internalGraph;

  double accX;
  double accY;
  double accZ;

  double rotX;
  double rotY;
  double rotZ;

  NSArray *pressureData;
}

- (instancetype)initWithGraph:(CPTGraph *)graph graphDataSource:(GraphDataSource *)source;

@end