//
// Created by Kowser on 11/26/14.
// Copyright (c) 2014 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "WifiStreamReader.h"

@implementation WifiStreamReader

- (instancetype)init
{
  NSAssert(NO, @"init not allowed");
  return nil;
}

- (instancetype)initWithGraph:(CPTGraph *)graph graphDataSource:(GraphDataSource *)dataSource
{
  self = [super init];
  if (self) {
    csvFilePath = [self getCsvFilePath];
    pressureData = @[@0, @0, @0, @0];
    internalGraph = graph;
    graphDataSource = dataSource;
    simplePing = [SimplePing simplePingWithHostName:@"169.254.1.1"];
    simplePing.delegate = self;
    [simplePing start];

    stringBuffer = [NSMutableString string];
    [self connectToTarget];

    motionManager = [[CMMotionManager alloc] init];
    motionManager.accelerometerUpdateInterval = .2;
    motionManager.gyroUpdateInterval = .2;

    [motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                        withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
                                            [self outputAccelerationData:accelerometerData.acceleration];
                                            if (error) {
                                              NSLog(@"%@", error);
                                            }
                                        }];

    [motionManager startGyroUpdatesToQueue:[NSOperationQueue currentQueue]
                               withHandler:^(CMGyroData *gyroData, NSError *error) {
                                   [self outputRotationData:gyroData.rotationRate];
                               }];
  }
  return self;
}

- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)eventCode
{
  switch (eventCode) {
    case NSStreamEventErrorOccurred: {
      NSLog(@"Here @Event Error Occured");
      break;
    }
    case NSStreamEventHasBytesAvailable: {
      NSLog(@"Here @Bytes Available");
      [self readIntoBuffer];
      break;
    }
    case NSStreamEventHasSpaceAvailable: {
      NSLog(@"Here 1@Space Available");
      break;
    }
    case NSStreamEventOpenCompleted: {
      NSLog(@"Here @Event Open Completed");
      break;
    }
    default:
      NSLog(@"default @Stream");
      break;
  }
}

- (Boolean)connectToTarget
{
  Boolean isSuccessful = YES;
  CFReadStreamRef read = NULL;
  CFWriteStreamRef write = NULL;

  CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (CFStringRef) @"169.254.1.1", 80, &read, &write);

  if (read && write) {
    inpStream = objc_retainedObject(read);
    [inpStream setDelegate:self];
    [inpStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inpStream open];

    outputStream = objc_retainedObject(write);
    [outputStream setDelegate:self];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream open];

    if ([inpStream hasBytesAvailable]) {
      [self readIntoBuffer];
    }
    NSLog(@"Connected!!");
  }
  else {
    isSuccessful = NO;
    NSLog(@"Not Connected");
  }


  if (read) {
    CFRelease(read);
  }
  if (write) {
    CFRelease(write);
  }

  return isSuccessful;
}

- (void)readIntoBuffer
{
  NSLog(@"Here @ read into buffer");

  uint8_t buffer[1024];
  unsigned int len = 0;

  if ([inpStream hasBytesAvailable]) {
    len = (unsigned int) [inpStream read:buffer maxLength:1023];
    if(len > 0) {
      NSString *string = [[NSString alloc] initWithFormat:@"%s", buffer];
      [stringBuffer appendString:string];
    }
  }

  NSRange range;
  range.location = 0;
  NSUInteger length = 0;
  while ((length = [stringBuffer rangeOfString:@"]"].location) != NSNotFound) {
    range.length = length + 1;
    NSString *result = [stringBuffer substringToIndex:range.length];
    [stringBuffer deleteCharactersInRange:range];

    result = [result stringByReplacingOccurrencesOfString:@"[" withString:@""];
    result = [result stringByReplacingOccurrencesOfString:@"]" withString:@""];
    [self readPressureData:result];
    [self saveData];
    [self setDataForPlot];

    [internalGraph reloadData];
  }
}

- (void)outputAccelerationData:(CMAcceleration)acceleration
{
  accX = acceleration.x;
  accY = acceleration.y;
  accZ = acceleration.z;

  NSLog(@"Acceleration: %.3fg %.3fg %.3fg", accX, accY, accZ);
}

- (void)outputRotationData:(CMRotationRate)rotation
{
  rotX = rotation.x;
  rotY = rotation.y;
  rotZ = rotation.z;

  NSLog(@"Rotation: %.3fr/s %.3fr/s %.3fr/s", rotX, rotY, rotZ);
}

- (void)readPressureData:(NSString *)resultString
{
  @try {
    NSArray *analogValues = [resultString componentsSeparatedByString: @","];
    if(analogValues.count == 4) {
      pressureData = analogValues;
    }
  }
  @catch (NSException *exception) {
    NSLog(@"Exception: %@", exception.reason);
  }
  @finally {
  }
}

- (void)setDataForPlot
{
  [graphDataSource addDataForPlot:@"rotX" data:rotX];
  [graphDataSource addDataForPlot:@"rotY" data:rotY];
  [graphDataSource addDataForPlot:@"rotZ" data:rotZ];

  [graphDataSource addDataForPlot:@"accX" data:accX];
  [graphDataSource addDataForPlot:@"pressure1" data:([pressureData[0] doubleValue] / 1000)];
}

- (void)saveData
{
  NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"hh-mm-ss SSS"];
  NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];

  NSString *fileData = [[NSString alloc] initWithFormat:@"%@,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,\n", dateString,
                                                        ([pressureData[0] doubleValue] / 1000), ([pressureData[1] doubleValue] / 1000),
                                                        ([pressureData[2] doubleValue] / 1000), ([pressureData[3] doubleValue] / 1000),
                                                        accX, accY, accZ, rotX, rotY, rotZ];
  [self saveTextToFile:fileData];
}

- (void)saveTextToFile:(NSString *)stringData
{
  NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:csvFilePath];
  [fileHandle seekToEndOfFile];
  [fileHandle writeData:[stringData dataUsingEncoding:NSUTF8StringEncoding]];
  [fileHandle closeFile];
}

- (NSString *)getCsvFilePath
{
  NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *documentsDirectory = documentPaths[0];
  NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"data.txt"];
  NSLog(filePath);

  if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
    [[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];
  }
  return filePath;
}

@end
