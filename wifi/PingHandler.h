//
//  PingHandler.h
//  SimplePingTest
//
//  Created by Miftah on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimplePing.h"

@interface PingHandler : NSObject<SimplePingDelegate, NSStreamDelegate>
{
@private
    NSInputStream * inpStream;
    NSOutputStream * outpStream;
    NSMutableData * dataBuffer;
    
    id myDelegate;
    SimplePing * mySimplePing;
}

- (id) init;

- (void) simplePing:(SimplePing *)pinger didStartWithAddress:(NSData *)address;
- (void) simplePing:(SimplePing *)pinger didSendPacket:(NSData *)packet;
- (void) simplePing:(SimplePing *)pinger didReceivePingResponsePacket:(NSData *)packet;

- (Boolean) connectToTarget;

@end
