//
//  PingHandler.m
//  SimplePingTest
//
//  Created by Miftah on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PingHandler.h"
#import <CFNetwork/CFSocketStream.h>
#import "SimplePing.h"

@implementation PingHandler

- (id) init
{
    mySimplePing = [SimplePing simplePingWithHostName:@"169.254.1.1"];
    mySimplePing.delegate = self;
    [mySimplePing start];
    
    return self;
}

- (void) simplePing:(SimplePing *)pinger didStartWithAddress:(NSData *)address
{
    
}

- (void) simplePing:(SimplePing *)pinger didSendPacket:(NSData *)packet
{
    
}

-(void) simplePing:(SimplePing *)pinger didReceivePingResponsePacket:(NSData *)packet
{
    
}

- (Boolean) connectToTarget
{
    //    NSLog(@"Here @ connect to target");
    
    Boolean was_successfull = YES;
    CFReadStreamRef read = NULL;
    CFWriteStreamRef write = NULL;
    
    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (CFStringRef) @"169.254.1.1", 80, &read, &write);
    
    if(read && write)
    {
        inpStream = objc_retainedObject(read);
        //[inpStream retain];
        [inpStream setDelegate:self];
        [inpStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [inpStream open];
        
        outpStream = objc_retainedObject(write);
        //[outpStream retain];
        [outpStream setDelegate:self];
        [outpStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [outpStream open];
        
        if( [ inpStream hasBytesAvailable] )
        {
            [ self readIntoBuffer ];
        }
        //connectionState = ConnectedToWiFly;
        // showRssiInQueue = YES;
        NSLog(@"Connected!!");
    }
    else
    {
        was_successfull = NO;
        NSLog(@"Not Connected");
    }
    
    
    if(read)
    {
        CFRelease(read);
    }
    if (write) 
    {
        CFRelease(write);
    }
    
    return was_successfull;
}


- (void) readIntoBuffer
{
    NSLog(@"Here @ read into buffer");
    
    uint8_t buffer[1024];
    unsigned int len = 0;
    
    if([inpStream hasBytesAvailable])
        len = [inpStream read:buffer maxLength:1023];
    
    if(len)
    {
        buffer[len+1] = '\0';
        [dataBuffer replaceBytesInRange:NSMakeRange(0, len+1) withBytes:buffer];
    }
    //    else
    //        buffer[0]=0;
    
    NSLog(@"Buffer = %s", buffer);
}

@end
